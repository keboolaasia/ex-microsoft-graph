from setuptools import setup, find_packages
import exmsgraph
setup(
    name='exmsgraph',
    version=exmsgraph.__version__,
    url='https://bitbucket.org/keboolaasia/ex-microsoft-graph',
    download_url='https://bitbucket.org/keboolaasia/ex-microsoft-graph',
    packages=find_packages(),
    install_requires=[
        'requests'
    ]
)
