"""

Sample config:
{
  "carpark_reports_dir": "/TECLKeboola/Carpark Reports",
  "how": "all",
  "client_id": "",
  "drive_root_path": "/users/itsupport@esplanade.com/drive/root",
  "#refresh_token": "",
}
"""
import time
import datetime
import logging
import requests
from urllib.parse import urljoin, urlencode


class AuthError(Exception):
    pass


class GraphClient:
    def __init__(self, client_id, refresh_token=None, access_token=None,
                 authorization_code=None, tenant='common', client_secret=None,
                 redirect_uri=None, drive_root_path=None,
                 scope='files.read files.read.all offline_access user.read sites.read.all'):
        """https://developer.microsoft.com/en-us/graph/docs/concepts/auth_v2_user
        https://dev.onedrive.com/misc/addressing.htm

        The authentization works as follows:
        If you supply authorization_code it will be used to get refresh token
        If you supply refresh_token, the authorization code doesn't matter and
        the refresh_token will be used to get the access_token

        If you don't have neither authorization_code nor refresh_token

        >>> client = GraphClient(client_id) # optionally the scope parameter
        >>> client.authorize()
        url will show here, go to the url, sign in
        You will be redirected to localhost/something.
        In the redirected url, copy the authorization_code and use it to authenticate

        !!!
        After each exchange of the refresh_token for an access_token, the
        refresh_token itself is refreshed, so make sure to save the latest
        refresh token by accessing GraphClient.refresh_token
        !!!

        Each refresh_token lasts for about 14 days

        If you wish to call different graph api resources, use the
        GraphClient.auth_get() method


        Args:
            client_id: obtained from registering the app
                @ https://apps.dev.microsoft.com/portal/register-app
            tenant: Not really sure what it is, but according to the docs 'common'
                should do
            drive_root_path (str): if you plan to access drive with this client, fill this.
                its a path the users drive, ex: /users/mail@provider.com/drive/root

        https://blogs.msdn.microsoft.com/sharepointdev/2013/08/13/access-onedrive-for-business-using-the-sharepoint-2013-apis/

        """

        self._logger = logging.getLogger(__name__)
        self._scope = scope
        self._root_url = 'https://graph.microsoft.com/v1.0/'
        self._redirect_uri = redirect_uri

        self.client_id = client_id
        self.refresh_token = refresh_token
        self._authorization_code = authorization_code
        self.client_secret = client_secret
        self._tenant = tenant

        self._token_url = ('https://login.microsoftonline.com/'
                           '{tenant}/oauth2/v2.0/token'.format(tenant=self._tenant))

        self._access_token = access_token

        self._token_created_at = datetime.datetime.now() if access_token is not None else None
        self._ttl = 3600

        self.drive_root_path = drive_root_path.lstrip('/')

        # Get info about the actual folder/file in drive located at `path`
        self._drive_resource_url = '{root}:{{path}}:/'.format(root=drive_root_path)

        # Show contents of a folder `path` in drive
        self._drive_contents_url =  self._drive_resource_url + 'children?select=name'


    def _token_alive_for(self):
        return (datetime.datetime.now() - self._token_created_at).seconds

    def invalidate_access_token(self):
        """Invalidate access token"""
        self._token_created_at = datetime.datetime(1999, 1, 1)

    def authorize(self):
        url = 'https://login.microsoftonline.com/{tenant}/oauth2/authorize?'.format(tenant=self._tenant)
        params = dict(
            client_id=self.client_id,
            response_type='code',
            state='foobar1234',
            scope=self._scope,
            redirect_uri=self._redirect_uri,
            response_mode='query')
        authorization_url = (url + urlencode(params)).replace('+', '%20')
        print("Go to {} in your browser to get user consent".format(authorization_url))

    def adminconsent(self):
        url = ('https://login.microsoftonline.com/{tenant}/adminconsent?'
               'client_id={appid}&state={state}&redirect_uri={redirect_uri}'.format(
                   tenant=self._tenant,
                   appid=self.client_id,
                   state='foobar1234',
                   redirect_uri='http://localhost/app'))
        print("Go to {} in your browser to get admin consent".format(url))

    def _is_access_token_alive(self):
        """Check the validity of a token

        Substract 100 from time to live (ttl) just to be sure
        """
        try:
            delta = self._token_alive_for()
        except TypeError:
            # make token dead on error
            delta = self._ttl + 999
        if delta > self._ttl - 100:
            return False
        else:
            return True

    def _get_access_token(self):
        self._logger.info("Getting new access token")
        payload = {
            'client_id': self.client_id,
            'scope': self._scope,
            'redirect_uri': self._redirect_uri,
            'client_secret': self.client_secret
            }
        if self.refresh_token is None:
            payload['grant_type'] = 'authorization_code',
            if self._authorization_code is None:
                raise ValueError("If you don't have refresh_token, pass in"
                                 " the authorization_code")
            payload['code'] = self._authorization_code
        else:
            payload['grant_type'] = 'refresh_token'
            payload['refresh_token'] = self.refresh_token

        _resp = requests.post(self._token_url, data=payload)
        try:
            _resp.raise_for_status()
        except:
            self._logger.error(_resp.text)
            raise
        else:
            resp =  _resp.json()
            logging.info("Successfully authenticated")
            self._token_created_at = datetime.datetime.now()
            logging.info("Updated the refresh token")
            self.refresh_token = resp['refresh_token']
            self._access_token = resp['access_token']

    @property
    def access_token(self):
        """
        Get or reuse an auth token from microsoft oauth2 api.

        Returns:
            oauth2 api token
        https://developer.microsoft.com/en-us/graph/docs/concepts/auth_v2_service
        """
        if not self._is_access_token_alive():
            self._get_access_token()
        return self._access_token

    def auth_get(self, endpoint, *args, **kwargs):
        """Make an authorized get reuqest"""
        url = self._root_url + endpoint.lstrip('/')
        headers = {'Authorization': 'Bearer {}'.format(self.access_token)}
        resp = requests.get(url, headers=headers, *args, **kwargs)
        try:
            resp.raise_for_status()
        except:
            self._logger.error(resp.text)
            raise
        return resp

    def drive_folder_contents(self, path):
        """Given path to folder, show it's contents"""

        return self.auth_get(self._drive_contents_url.format(path=path)).json()

    def drive_file_details(self, drive_path):
        """List informaions on the drive resource (folder, file)

        Args:
            drive_path (str): /Path/to/resource/on_drive.xlsx
                returns for example the download link, modified status etc.

        Returns:
            json object with resource metadata
        """
        return self.auth_get(self._drive_resource_url.format(path=drive_path)).json()

    def download_drive_file(self, drive_path, local_path):
        self._logger.info("Downloading %s to %s", drive_path, local_path)
        data = self.drive_file_details(drive_path)
        dl_url = data.get('@microsoft.graph.downloadUrl')
        if dl_url is None:
            raise ValueError("Resource {} is not available!\n{}".format(drive_path, data))
        else:
            time.sleep(2)
            self._logger.debug("Drive link url is %s", dl_url)
            target_file = requests.get(dl_url, stream=True)
            with open(local_path, 'wb') as outf:
                for chunk in target_file.iter_content(chunk_size=2048):
                    if chunk:
                        outf.write(chunk)
            return local_path

    def me(self):
        return self.auth_get('me')
