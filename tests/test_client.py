import pytest
from exmsgraph.client import GraphClient, AuthError
import datetime

class FakeResp:
    def __init__(self, code, response):
        self.status_code = code
        self.response = response
        self.text = 'Dummy response text'

    def raise_for_status(self):
        if self.status_code != 200:
            raise AuthError("Wrong auth")
    def json(self, *args, **kwargs):
        return self.response

def fake_post_ok(*args, **kwargs):
    return FakeResp(code=200, response={'access_token': 'secret', 'refresh_token': 'refreshME'})

def fake_post_ok2(*args, **kwargs):
    return FakeResp(code=200, response={'access_token': 'secret2', 'refresh_token': 'refreshME2'})

def fake_post_wrong(*args, **kwargs):
    return FakeResp(code=400, response={'error_message': 'something wrong'})

def test_authenticating_client_first_time(monkeypatch):
    c = GraphClient(client_id='foo', authorization_code = 'bar', drive_root_path='nutin/')
    assert c.refresh_token is None
    assert c._access_token is None

    monkeypatch.setattr('requests.post', fake_post_ok)
    assert c.access_token == 'secret'
    assert c.refresh_token == 'refreshME'

def test_bad_authentication_raises(monkeypatch):
    c = GraphClient('foo', 'bar', drive_root_path='nutin/')

    monkeypatch.setattr('requests.post', fake_post_wrong)
    with pytest.raises(AuthError):
        c.access_token

def test_authenticating_client_dead_token(monkeypatch):
    c = GraphClient('foo', refresh_token ='bar', drive_root_path='nutin/')
    assert c.refresh_token == 'bar'
    monkeypatch.setattr('requests.post', fake_post_ok)
    assert c.access_token == 'secret'
    assert c.refresh_token == 'refreshME'
    c._token_created_at = datetime.datetime.now() - datetime.timedelta(hours=4)
    monkeypatch.setattr('requests.post', fake_post_ok2)
    assert c.access_token == 'secret2'
    assert c.refresh_token == 'refreshME2'

def test_checking_for_dead_token():
    c = GraphClient('foo', 'bar', drive_root_path='nutin/')
    assert c._is_access_token_alive() is False

    c._token_created_at = datetime.datetime.now() - datetime.timedelta(hours=4)
    assert c._is_access_token_alive() is False

    c._token_created_at = datetime.datetime.now()
    assert c._is_access_token_alive() is True
